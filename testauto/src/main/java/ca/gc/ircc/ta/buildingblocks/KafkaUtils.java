package ca.gc.ircc.ta.buildingblocks;

import java.time.Duration;
import java.util.Collections;
import java.util.Properties;

import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.Callback;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.apache.log4j.Logger;

import ca.gc.ircc.hip.integration.familyclass.applicationassessment;
import io.confluent.kafka.serializers.KafkaAvroDeserializer;
import io.confluent.kafka.serializers.KafkaAvroSerializer;

public class KafkaUtils {
	String offsetMetadata = "NoData";
	final static Logger logger = Logger.getLogger(KafkaUtils.class);

	public String writeEventToTopic(String BoootStrap_Server, String SchemaRegistry_Server,
			applicationassessment AppAssessment_Payload, String Topic_Name) {

		Properties properties = new Properties();
		properties.setProperty("bootstrap.servers", BoootStrap_Server);
		properties.setProperty("acks", "1");
		properties.setProperty("retries", "10");
		properties.setProperty("key.serializer", StringSerializer.class.getName());
		properties.setProperty("value.serializer", KafkaAvroSerializer.class.getName());
		properties.setProperty("schema.registry.url", SchemaRegistry_Server);

		KafkaProducer<String, applicationassessment> kafkaProducer = new KafkaProducer<String, applicationassessment>(
				properties);

		ProducerRecord<String, applicationassessment> producerRecord = new ProducerRecord<String, applicationassessment>(
				Topic_Name, AppAssessment_Payload);
		kafkaProducer.send(producerRecord, new Callback() {
			@Override
			public void onCompletion(RecordMetadata recordMetadata, Exception e) {
				if (e == null) {
					logger.info("Message has been successfully posted to topic " + Topic_Name + " metadata"
							+ recordMetadata.toString());
					offsetMetadata = recordMetadata.toString();

				} else {
					e.printStackTrace();

				}

			}

		});

		kafkaProducer.flush();
		kafkaProducer.close();
		return offsetMetadata;
	}

	public applicationassessment readEventfromTopic(String BoootStrap_Server, String SchemaRegistry_Server,
			int Partition, int OffSet, String Topic_Name) {
		final Duration POLL_TIMEOUT = Duration.ofSeconds(1);

		Properties properties = new Properties();
	    properties.setProperty("bootstrap.servers", BoootStrap_Server);
	    properties.setProperty("group.id","my-avro-consumer");
	    properties.setProperty("enable.auto.commit", "false");
	    properties.setProperty("auto.offset.reset", "earliest");
	    properties.setProperty("key.deserializer", StringDeserializer.class.getName());
	    properties.setProperty("value.deserializer", KafkaAvroDeserializer.class.getName());
	    properties.setProperty("schema.registry.url",SchemaRegistry_Server);
	    properties.setProperty("specific.avro.reader", "true");

	        KafkaConsumer<String, applicationassessment> consumer=new KafkaConsumer<String, applicationassessment>(properties);
	        
	        TopicPartition topicPartition = new TopicPartition(Topic_Name, Partition);
	        consumer.assign(Collections.singletonList(topicPartition));
	        consumer.seek(topicPartition, OffSet);
	        ConsumerRecords<String, applicationassessment> record = consumer.poll(100);
	        applicationassessment app=record.iterator().next().value();
	        //System.out.println(app);
	        //consumer.commitSync();		
	        return app;
	        
	        
	       
	     
	}

}
