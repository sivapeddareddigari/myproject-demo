package ca.gc.ircc.ta.buildingblocks;

import java.util.Map;

public class EnvMap {
    public static void main (String[] args) {
        Map<String, String> env = System.getenv();
        System.out.println(System.getenv("BOOTSTRAP_SERVER"));
        System.out.println(System.getenv("SCHEMA_REGISTRY_URL"));
       
        for (String envName : env.keySet()) {
            System.out.format("%s=%s%n",
                              envName,
                              env.get(envName));
        }
    }
}