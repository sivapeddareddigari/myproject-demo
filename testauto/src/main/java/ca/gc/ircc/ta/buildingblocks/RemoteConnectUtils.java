package ca.gc.ircc.ta.buildingblocks;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.log4j.Logger;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpATTRS;
import com.jcraft.jsch.SftpException;

public class RemoteConnectUtils {
	final static Logger logger = Logger.getLogger(RemoteConnectUtils.class);
	private FTPClient myFTPClient;
	public JSch jsch;
	public static Session session;
	public static Channel channel;
	public static ChannelSftp sftpChannel;
	ReadTestdata rt = new ReadTestdata();
	WriteTestData wt = new WriteTestData();
	int totalResults = 0;

	// Connects to a remote FTP server

	public ChannelSftp connectSFTP(String Host, int Port, String UserID, String Password) throws SftpException {

		try {
			JSch jsch = new JSch();
			session = jsch.getSession(UserID, Host, Port);

			session.setPassword(Password);
			java.util.Properties config = new java.util.Properties();

			config.put("StrictHostKeyChecking", "no");
			config.put("PreferredAuthentications", "publickey,keyboard-interactive,password");
			session.setConfig(config);
			session.connect();
			channel = session.openChannel("sftp");

			channel.connect();
			sftpChannel = (ChannelSftp) channel;

		} catch (JSchException e) {

			System.out.println(e);
			e.printStackTrace();
		}
		return sftpChannel;

	}

	public ChannelSftp connectSFTP(String Host, int Port, String UserID, String Password, boolean NoKerberos)
			throws SftpException {

		try {
			JSch jsch = new JSch();
			session = jsch.getSession(UserID.trim(), Host, Port);

			session.setPassword(Password);
			java.util.Properties config = new java.util.Properties();

			config.put("StrictHostKeyChecking", "no");
			config.put("PreferredAuthentications", "publickey,keyboard-interactive,password");
			session.setConfig(config);
			session.connect();
			channel = session.openChannel("sftp");

			channel.connect();
			sftpChannel = (ChannelSftp) channel;

		} catch (JSchException e) {

			System.out.println(e);
			e.printStackTrace();
		}
		return sftpChannel;

	}

	public ChannelSftp connectSFTP(String Host, int Port, String UserID) throws SftpException {

		try {
			JSch jsch = new JSch();
			session = jsch.getSession(UserID, Host, Port);
			// session.setPassword(Password);
			java.util.Properties config = new java.util.Properties();

			config.put("StrictHostKeyChecking", "no");

			session.setConfig(config);
			session.connect();
			channel = session.openChannel("sftp");
			channel.connect();
			sftpChannel = (ChannelSftp) channel;

		} catch (JSchException e) {

			System.out.println(e);
			e.printStackTrace();
		}
		return sftpChannel;

	}

	public boolean checkDirectoryExists(ChannelSftp sftpChannel, String someDir) throws IOException, SftpException {

		try {

			someDir = sftpChannel.pwd() + "/" + someDir;
		} catch (SftpException e) {
			return false;
		}

		return true;
	}

	// check whether a file exists or not
	public boolean checkFileExists(ChannelSftp channelSftp, String Directory, String fileName) throws Exception {
		SftpATTRS attrs = null;
		try {
			String path = Directory.trim() + "/" + fileName.trim();

			channelSftp.ls(path);
			attrs = channelSftp.stat(path);
			System.out.println(attrs.getSize());
			return false;
		} catch (Exception e) {
			System.out.print(e.getMessage());
			System.out.println("  Success! " + fileName + " does not exist in " + Directory);
			return true;
		}

	}

	
	public boolean checkFileExists(ChannelSftp channelSftp, String Directory, String fileName, String Destination) {

		try {
			String path = Directory.trim() + "/" + fileName.trim();

			channelSftp.ls(path);
			return true;
		} catch (Exception e) {

			logger.info(e.getMessage() + " at: " + Directory + "/" + fileName);

			return false;
		}
	}
	// Close the connection
	public void logout(FTPClient fc) throws IOException {
		if (fc != null && myFTPClient.isConnected()) {
			fc.logout();
			fc.disconnect();
			logger.info("Connection to FTP server disconnected and logged out");
		}
	}

	public boolean upload(ChannelSftp sftpChannel, String fileName, String remoteDir) throws SftpException {

		FileInputStream fis = null;
		boolean retVal;
		// connectSFTP();
		try {

			// Change to output directory
			sftpChannel.cd(remoteDir);
			// Upload file
			File file = new File(fileName);
			fis = new FileInputStream(file);
			sftpChannel.put(fis, file.getName());
			fis.close();
			retVal = true;

		} catch (Exception e) {
			System.out.println(e);
			// e.printStackTrace();
			retVal = false;
		}

		return retVal;

	}

	public boolean DeleteAllinDir(ChannelSftp sftpChannel, String fileName, String remoteDir) throws SftpException {
		String[] FileColStr = null;
		String FName = new String();

		FileColStr = rt.SplitString(fileName);
		for (int i = 0; i < FileColStr.length; i++) {
			FName = FName + " " + FileColStr[i].substring(0, FileColStr[i].indexOf(',')) + "*";
		}
		FName = FName.trim();

		boolean retVal;
		// connectSFTP();
		try {

			sftpChannel.cd(remoteDir);
			sftpChannel.rm(FName);

			retVal = true;

		} catch (Exception e) {
			System.out.println(e);
			// e.printStackTrace();
			retVal = false;
		}

		return retVal;

	}

	public static File download(ChannelSftp sftpChannel, String fileName, String localDir) throws SftpException {

		byte[] buffer = new byte[1024];
		BufferedInputStream bis;
		File newFile = null;
		try {
			// Change to output directory
			String cdDir = fileName.substring(0, fileName.lastIndexOf("/") + 1);
			sftpChannel.cd(cdDir);
			System.out.println(":: localDir :: " + localDir + " :: fileName:: " + fileName + ":: cdDir :: " + cdDir);
			File file = new File(fileName);
			bis = new BufferedInputStream(sftpChannel.get(file.getName()));

			newFile = new File(localDir + "/" + file.getName());

			// Download file
			OutputStream os = new FileOutputStream(newFile);
			BufferedOutputStream bos = new BufferedOutputStream(os);
			int readCount;
			while ((readCount = bis.read(buffer)) > 0) {
				bos.write(buffer, 0, readCount);
			}
			bis.close();
			bos.close();
			System.out.println("File downloaded successfully - " + file.getAbsolutePath());

		} catch (Exception e) {
			e.printStackTrace();
		}
		logger.info("File: " + newFile + " has been downloaded! ");
		return newFile;
		// disconnect();
	}

	public String getFileMD5Digest(File sFile) throws IOException {
		String fileMD5 = null;
		FileInputStream fis;

		try {
			fis = new FileInputStream(sFile);
			fileMD5 = DigestUtils.md5Hex(fis);
			fis.close();
			return fileMD5;

		} catch (IOException e) {
		
			e.printStackTrace();
		}
		return fileMD5;

	}

	public void purgeTempDirectory(String Dir) {
		File file = new File(Dir);
		String[] myFiles;
		if (file.isDirectory()) {
			myFiles = file.list();
			for (int i = 0; i < myFiles.length; i++) {
				File myFile = new File(file, myFiles[i]);
				myFile.delete();
			}

		}
	}

	
}
