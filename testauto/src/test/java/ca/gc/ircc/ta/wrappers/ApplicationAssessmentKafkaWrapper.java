package ca.gc.ircc.ta.wrappers;

import ca.gc.ircc.ta.buildingblocks.KafkaUtils;
import ca.gc.ircc.ta.buildingblocks.ReadTestdata;
import ca.gc.ircc.hip.integration.familyclass.applicationassessment;
import ca.gc.ircc.ta.buildingblocks.RemoteConnectUtils;
import ca.gc.ircc.ta.buildingblocks.RestAssuredUtil;
import io.restassured.response.Response;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import java.io.File;
import java.io.FileInputStream;
import java.text.SimpleDateFormat;
import java.util.HashMap;

import javax.json.JsonObject;


public class ApplicationAssessmentKafkaWrapper {
    long Sdate;
    long FDate;
    final static Logger perf = Logger.getLogger("perf");
    final static Logger file = Logger.getLogger("file");
    boolean scenarioStatus = true;
    public static final String pattern = "dd/MM/yyyy HH:mm:ss SS";
    public static final SimpleDateFormat format = new SimpleDateFormat(pattern);
    final static Logger logger = Logger.getLogger(apitestingwrapper.class);
    RemoteConnectUtils fcutils = new RemoteConnectUtils();
    ReadTestdata rt = new ReadTestdata();
    KafkaUtils ku = new KafkaUtils();
    
    RestAssuredUtil rut = new RestAssuredUtil();
    Response res;
    String ScenarioDescription;
    String DataFileLocalFolder;
    int WaitNumber;
    String ScenarioType;
    String InputTopicName;
    String OutputTopicName;
    String InputTopicAvroSchemaDefFile;
    String OutputTopicAvroSchemaDefFile;
    String InputTopicPayloadFile;
    String OutputTopicPayloadFile;
    String DBConnectionString;
    String DBCollectionName;
    String BootStrapServer;
    String RegistryServerUrl;
    String offsetMetadata="NoData";
    




    @Parameters({"ScenarioName", "TestDataFileName"})

    @BeforeClass
    public void BootStrap(String ScenarioName, String TestDataFileName) {
        try {
            BasicConfigurator.configure();
            HashMap<String, String> ScenarioData = new HashMap<String, String>();
            JsonObject InputPayloadData = null;
            String log4jConfPath = "resources/log4j.properties";
            PropertyConfigurator.configure(log4jConfPath);

            Sdate = System.currentTimeMillis();

            if (TestDataFileName.contains(".xlsx")) {
                XSSFWorkbook workbook;
                FileInputStream finput = null;
                XSSFSheet sheet;
                File src = new File(TestDataFileName);

                try {
                    finput = new FileInputStream(src);

                    System.out.println(".....finput initialized");

                    workbook = new XSSFWorkbook(finput);
                    System.out.println(".....Workbook initialized");
                    sheet = workbook.getSheet("Initial");
                    System.out.println(".....initial Sheet initialized");

                    ScenarioData = rt.getScenarioData(workbook, sheet, ScenarioName);

                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else if (TestDataFileName.contains(".json")) {
                try {
                    ScenarioData = rt.readInputFromJSON("Queue", TestDataFileName, ScenarioName);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            DataFileLocalFolder= ScenarioData.get("DataFileLocalFolder");
            WaitNumber = Integer.parseInt(ScenarioData.get("WaitNumber"));
            ScenarioType= ScenarioData.get("ScenarioType");
            InputTopicName= ScenarioData.get("InputTopicName");
            OutputTopicName = ScenarioData.get("OutputTopicName");
            InputTopicAvroSchemaDefFile= ScenarioData.get("InputTopicAvroSchemaDefFile");
            OutputTopicAvroSchemaDefFile = ScenarioData.get("OutputTopicAvroSchemaDefFile");
            InputTopicPayloadFile = ScenarioData.get("InputTopicPayloadFile");
            OutputTopicPayloadFile = ScenarioData.get("OutputTopicPayloadFile");
            DBConnectionString = ScenarioData.get("DBConnectionString");
            DBCollectionName = ScenarioData.get("DBCollectionName");
            BootStrapServer = System.getenv("BOOTSTRAP_SERVER");
            RegistryServerUrl = System.getenv("SCHEMA_REGISTRY_URL");
            
            ScenarioDescription = ScenarioData.get("ScenarioDescription");
            logger.info("Scenarios which is Running ::" + ScenarioDescription);
            try {
            	 InputPayloadData= rt.readJSONFile(InputTopicPayloadFile);  
            	
            }catch (Exception e) {
            	e.printStackTrace();
            	
            }
            
            
            applicationassessment appas = applicationassessment.newBuilder()
            		.setCorrelationId(InputPayloadData.getString("CorrelationId"))
            		.setApplicationCategory(InputPayloadData.getInt("ApplicationCategory"))
            		.setApplicationNumber(InputPayloadData.getString("ApplicationNumber"))
            		.setUci(InputPayloadData.getInt("Uci"))
            		.setType(InputPayloadData.getInt("Type"))
            		.setStatus(InputPayloadData.getInt("Status"))
            		.setStatusUpdatedBy(InputPayloadData.getString("CorrelationId"))
            		.setRefusalGrounds(InputPayloadData.getString("RefusalGrounds"))
            		.setStatusDate(Integer.parseInt(InputPayloadData.getString("StatusDate")))
            		.setActivityNumber(InputPayloadData.getString("ActivityNumber"))
            		.build();
            
            offsetMetadata=ku.writeEventToTopic(BootStrapServer, RegistryServerUrl, appas, InputTopicName);
            	
          
            if (offsetMetadata != "NoData") {
            	logger.info("Message has been sent to Topic "+InputTopicName+" ::" + offsetMetadata);
            }else {
            	logger.info("Message was NOT sent to Topic "+InputTopicName);
            }
            
            

        } catch (Exception e) {
        	
        	logger.info(e.getMessage());
        }

    }
    
    

    @AfterClass
    public void Afterclassfunctionhere() {
        if (scenarioStatus) {

            FDate = System.currentTimeMillis();
            System.err.println("total time taken ~:" + (FDate - Sdate));
            perf.info(
                    ScenarioDescription + " | " + this.getClass().getName() + " | " + (FDate - Sdate));
        }

    }

    @Test(priority = 1, enabled = true)
    public void CheckEventinTopic() throws Exception {
    	logger.info("Checking the message: "+ offsetMetadata + " in topic: " + InputTopicName);
    	System.out.println(offsetMetadata);
    	logger.info("Waiting for for consumer to read the topic...");
		for (int i = 1; i < WaitNumber; i++) {

			System.out.print(i + " . ");
			Thread.sleep(500);

		}
		
		String OffStr = offsetMetadata.substring(offsetMetadata.lastIndexOf("-") + 1);
		
		int partition=Integer.parseInt(OffStr.substring(0, OffStr.indexOf("@")));
		
		int offset=Integer.parseInt(OffStr.substring(OffStr.lastIndexOf("@") + 1));
		
    	applicationassessment app = ku.readEventfromTopic(BootStrapServer, RegistryServerUrl, partition, offset, InputTopicName);
    	if (app != null) {
    		Assert.assertTrue(true);
    		
    	}else {
    		Assert.assertTrue(false);
    	}
    		
    	System.out.println(app.toString());
    	
    }

    
    @Test(priority = 2, enabled = true)
    public void CheckDocumentInCollection() {

    }


}
