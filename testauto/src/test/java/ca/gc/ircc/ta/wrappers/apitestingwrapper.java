package ca.gc.ircc.ta.wrappers;

import static org.testng.Assert.assertEquals;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.HashMap;

import javax.jms.Destination;
import javax.jms.JMSContext;

import org.apache.commons.net.ftp.FTPClient;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import org.testng.Assert;
import org.testng.SkipException;

import com.jcraft.jsch.ChannelSftp;

import ca.gc.ircc.ta.buildingblocks.ReadTestdata;
import ca.gc.ircc.ta.buildingblocks.RemoteConnectUtils;
import ca.gc.ircc.ta.buildingblocks.RestAssuredUtil;
import io.restassured.response.Response;

public class apitestingwrapper {
	public static final String pattern = "dd/MM/yyyy HH:mm:ss SS";
	public static final SimpleDateFormat format = new SimpleDateFormat(pattern);
	final static Logger logger = Logger.getLogger(apitestingwrapper.class);
	public FTPClient fc;
	public String hostname;
	public int port;
	public String username;
	public String password;
	public ChannelSftp sfCnl;
	public JMSContext queueConnectionContext;
	public Destination dataSendToQueue;
	RemoteConnectUtils fcutils = new RemoteConnectUtils();
	ReadTestdata rt = new ReadTestdata();
	RestAssuredUtil rut = new RestAssuredUtil();
	Response res;
	String BaseURI;
	String BasePath;
	String RequestType;
	String ContentType;
	String OutputContentType;
	String InputFilePath;
	String ExectedOutputFilePath;
	String ScenarioDescription;
	String EndpointName;
	String ResponseJSONSchemaFilePath;
	String ValidateAtSource;

	long Sdate;
	long FDate;
	final static Logger perf = Logger.getLogger("perf");
	final static Logger file = Logger.getLogger("file");
	boolean scenarioStatus = true;

	@Parameters({ "ScenarioName", "TestDataFileName" })

	@BeforeClass
	public void BootStrap(String ScenarioName, String TestDataFileName) {

		try {

			BasicConfigurator.configure();
			HashMap<String, String> ScenarioData = new HashMap<String, String>();
			String log4jConfPath = "resources/log4j.properties";
			PropertyConfigurator.configure(log4jConfPath);

			Sdate = System.currentTimeMillis();

			if (TestDataFileName.contains(".xlsx")) {
				XSSFWorkbook workbook;
				FileInputStream finput = null;
				XSSFSheet sheet;
				File src = new File(TestDataFileName);

				try {
					finput = new FileInputStream(src);

					System.out.println(".....finput initialized");

					workbook = new XSSFWorkbook(finput);
					System.out.println(".....Workbook initialized");
					sheet = workbook.getSheet("Initial");
					System.out.println(".....initial Sheet initialized");

					ScenarioData = rt.getScenarioData(workbook, sheet, ScenarioName);

				} catch (Exception e) {
					e.printStackTrace();
				}

			} else if (TestDataFileName.contains(".JSON")) {
				try {
					ScenarioData = rt.readInputFromJSON("Queue", TestDataFileName, ScenarioName);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			BaseURI = ScenarioData.get("BaseURI");
			BasePath = ScenarioData.get("BasePath");
			RequestType = ScenarioData.get("RequestType");
			ContentType = ScenarioData.get("ContentType");
			OutputContentType = ScenarioData.get("OutputContentType");
			InputFilePath = ScenarioData.get("InputFilePath");
			ExectedOutputFilePath = ScenarioData.get("ExpectedOutputFilePath");
			ScenarioDescription = ScenarioData.get("ScenarioDescription");
			EndpointName = ScenarioData.get("EndpointName");
			ResponseJSONSchemaFilePath = ScenarioData.get("ResponseJSONSchemaFilePath");
			ValidateAtSource = ScenarioData.get("ValidateAtSource");
			logger.info("Scenarios which is Running ::" + ScenarioDescription);
			logger.info("Test Case is Running for Endpoint::" + EndpointName);
			FileInputStream fis = new FileInputStream(InputFilePath);
			logger.info("Requesting URI: " + BaseURI+ BasePath + " using: "+ RequestType + "  method...");
			res = rut.getResponse(BaseURI, BasePath, RequestType, ContentType, fis);
			logger.info("...Responded");
			

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@AfterClass
	public void Afterclassfunctionhere() {
		if (scenarioStatus) {

			FDate = System.currentTimeMillis();
			System.err.println("total time taken ~:" + (FDate - Sdate));
			perf.info(
					EndpointName + " | " + ScenarioDescription + " | " + this.getClass().getName() + " | " + (FDate - Sdate));
		}

	}

	@Test(priority = 1, enabled = true)
	public void CheckResponseCode() {
		logger.info("Checking the response code...");
		try {
			assertEquals(res.getStatusCode(), 200);
			logger.info("...response code is: "+ res.getStatusCode());

		} catch (Exception e) {
			logger.info("ERR...response code is: "+ res.getStatusCode());
			Assert.assertTrue(false);

		}
	}

	@Test(priority = 2, enabled = true)
	public void CheckResponseContent() {
		logger.info("Validating the "+ OutputContentType + " with expected structure...");
		
		try {
			File schema = new File(ResponseJSONSchemaFilePath);
			InputStream in = new FileInputStream(schema);
			assertEquals(true, rut.validateJSONStrcture(in, res.prettyPrint()));
			logger.info("...validated");

		} catch (Exception ex) {
			logger.info("ERR...validation failed");
			Assert.assertTrue(false);
			
		}

	}
	
	@Test(priority = 3, enabled = true)
	public void validateAtSource() {
		
		if(ValidateAtSource.equalsIgnoreCase("yes")) {
			
		
		logger.info("Validating at Source...");
		
		try {
			//TO Be Completed
			logger.info("...validated");
			Assert.assertTrue(true);

		} catch (Exception ex) {
			logger.info("ERR...validation failed");
			Assert.assertTrue(false);
			
		}
		}else {
			
			logger.info("Validation at source is not required and thus skipping this test...");
			throw new SkipException("...skipped executing");
			
		}

	}

}
